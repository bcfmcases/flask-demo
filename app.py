from flask import Flask
app = Flask(__name__)

@app.route('/app1')
def app1():
    return "v0.2 /app1 sayfası"

@app.route('/app2')
def app2():
    return "v0.2 /app2 sayfası"

@app.route('/')
def hello():
    return "v0.2 / sayfası"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)